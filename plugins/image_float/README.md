# Image Float Plugin

## About
This plugin adds three buttons to your toolbar which let you control the float of an image. They'll add classes: *float_left* and *float_right* or remove the float based classes. You can then style these in your stylesheets to set the text align.

## Installing
Before you follow the details below, you'll need to follow the generic plugin installation details: [https://github.com/wymeditor/wymeditor/wiki/Plugins](https://github.com/wymeditor/wymeditor/wiki/Plugins)

Add the following code to the CSS page for your website (where you're code will be output):
```
#!CSS
 .float_left{
	float: left;
 }
 .float_right{
	float: right;
 }
```

If you want to see these the images float your WYMEditor itself, you'll need to edit your skin file (the default is *wymeditor/iframe/default/wymiframe.css*) and add the above CSS too.