/*jslint evil: true */
/**
    WYMeditor.image_float
    ====================

    A plugin to add a class to an image which can be used to set its float
	
	by Patabugen ( patabugen.co.uk )
*/

WYMeditor.editor.prototype.image_float = function () {
    var wym = this,
		$box = jQuery(this._box);
		
	options = {
		
	}

	//construct the buttons' html
    var button_left = String() +
        "<li class='wym_tools_image_float_left'>" +
            "<a name='FloatLeft' href='#' " +
                "style='background-image: url(" +
                    wym._options.basePath +
                    "plugins/image_float/icons.png)'>" +
                "{left}" +
            "</a>" +
        "</li>";
    var button_right = String() +
        "<li class='wym_tools_image_float_right'>" +
            "<a name='FloatRight' href='#' " +
                "style='background-image: url(" +
                    wym._options.basePath +
                    "plugins/image_float/icons.png); background-position: 0px -24px'>" +
                "{right}" +
            "</a>" +
        "</li>";
    var button_none = String() +
        "<li class='wym_tools_image_float_none'>" +
            "<a name='FloatNone' href='#' " +
                "style='background-image: url(" +
                    wym._options.basePath +
                    "plugins/image_float/icons.png); background-position: 0px -48px'>" +
                "{none}" +
            "</a>" +
        "</li>";
		
	var html = button_left + button_right + button_none;
    //add the button to the tools box
    $box.find(wym._options.toolsSelector + wym._options.toolsListSelector)
        .append(html);
		
    $box.find('li.wym_tools_image_float_left a').click(function() {
		var container = wym.container();
		$(container).find('img').removeClass('float_left float_right');
		$(container).find('img').addClass('float_left');
		return false;
	});
    $box.find('li.wym_tools_image_float_right a').click(function() {
		var container = wym.container();
		$(container).find('img').removeClass('float_left float_right');
		$(container).find('img').addClass('float_right');
		return false;
	});
    $box.find('li.wym_tools_image_float_none a').click(function() {
		var container = wym.container();
		// Here we just remove the classes
		$(container).find('img').removeClass('float_left float_right');
		return false;
	});
};